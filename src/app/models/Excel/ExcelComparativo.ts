export interface ExcelComparativo {
    excelComparativoId?: number;

    excelComparativoNombre?: string;
  
    excelComparativoMes?: number;
  
    excelComparativoAnio?: number;
  
    excelTipoId?: number;

    usuarioId?: number;

    excelTipoPeriodo?:number;
  }